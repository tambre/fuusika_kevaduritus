% !TeX program = xelatex
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{vilgym}[version 0.15 Viljandi Gymnasium XeLaTeX document format]

% Enable LaTeX3
\RequirePackage{expl3}

\DeclareOption{draft}{\PassOptionsToPackage{\CurrentOption}{article}}
\ProcessOptions\relax

\LoadClass[12pt,a4paper]{article}

% Use Times New Roman. Requirex XeLaTeX.
\RequirePackage{fontspec}
\setmainfont{Times New Roman}

% Page margins
\RequirePackage{geometry}
\geometry{left=30mm, right=20mm, top=25mm, bottom=25mm}

% Page numbering
\RequirePackage{fancyhdr}
\pagenumbering{arabic}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\rfoot{\thepage}
\pagestyle{fancy}

% Set spacing
\RequirePackage{setspace}
\onehalfspacing

% For good automatic hyphenation
\usepackage[estonian]{babel}

% Start each section on a new page
\newcommand{\sectionbreak}{\newpage}

% Pragraph skip and indent
\setlength{\parskip}{12pt}
\setlength{\parindent}{0pt}

% Tweak skip for different list environments
\RequirePackage{enumitem}
\setlist[itemize]{topsep=0pt, itemsep=0pt}
\setlist[enumerate]{topsep=0pt, itemsep=0pt}
\setlist[description]{topsep=0pt, itemsep=0pt}

% Make description labels linkable
\let\originaldescriptionlabel\descriptionlabel
\renewcommand*{\descriptionlabel}[1]{\phantomsection\originaldescriptionlabel{#1}}

% Unnumbered sections
\newcommand*{\unsection}[1]{\newpage\phantomsection\section*{#1}\addcontentsline{toc}{section}{#1}}

% Modify caption label format
\RequirePackage{caption}
\captionsetup{justification=raggedright,singlelinecheck=false}
\captionsetup[figure]{name={Joonis},labelsep=period}
\captionsetup[table]{name={Tabel},labelsep=period}

% Make sure footnotes are properly left-aligned and at the bottom of the page
\RequirePackage[hang,flushmargin,bottom]{footmisc} 

% For TOC and title formatting
\RequirePackage{titlesec}
\RequirePackage{titletoc}

% Title formatting
\titleformat{\section}[hang]{\phantomsection\bfseries\uppercase}{\thesection}{12pt}{}
\titleformat{\subsection}[hang]{\bfseries}{\thesubsection}{12pt}{}

% TOC formatting
\renewcommand*{\contentsname}{\uppercase{SISUKORD}}
\dottedcontents{subsection}[40pt]{}{25pt}{4pt}
\dottedcontents{subsubsection}[75pt]{}{35pt}{4pt}
\titlecontents{section}[1pt]
              {}
              {\hspace*{15pt}\contentslabel{15pt}\uppercase}
              {\uppercase}
              {\titlerule*[4pt]{.}\contentspage}

% For indented examples and such
\newlength{\hangindentlength}
\newenvironment*{indented}[1]{#1\settowidth{\hangindentlength}{#1 }\setlength{\hangindent}{\hangindentlength}}{\par}

% Change reference section's language
\renewcommand*{\refname}{Viited}

% For having references section in the TOC
\usepackage[nottoc]{tocbibind}

% For extra math stuff
\usepackage{mathtools,amssymb}

% Allow breaking amsmath environments over pages.
\allowdisplaybreaks

% For SI units
\usepackage{siunitx}

% For fractions in SI values
\sisetup{quotient-mode=fraction}

% Fancy linking. Include last, so it can override other packages, if necessary.
\RequirePackage[hidelinks]{hyperref}

% Aggressive URL breaking. hyperref's default sucks.
\expandafter\def\expandafter\UrlBreaks\expandafter{\UrlBreaks% Save the current one
                                                   \do\a\do\b\do\c\do\d\do\e\do\f\do\g\do\h\do\i\do\j%
                                                   \do\k\do\l\do\m\do\n\do\o\do\p\do\q\do\r\do\s\do\t%
                                                   \do\u\do\v\do\w\do\x\do\y\do\z\do\A\do\B\do\C\do\D%
                                                   \do\E\do\F\do\G\do\H\do\I\do\J\do\K\do\L\do\M\do\N%
                                                   \do\O\do\P\do\Q\do\R\do\S\do\T\do\U\do\V\do\W\do\X%
                                                   \do\Y\do\Z}

% Some setup for titlepage configuration
\newtoks\type
\newtoks\authortext
\newtoks\instructortext
\newcommand*{\instructor}[1]{\instructortext{Juhendaja #1}}
\newcommand*{\instructors}[1]{\instructortext{Juhendajad #1}}
\renewcommand*{\author}[1]{\authortext{Autor #1}}
\newcommand*{\authors}[1]{\authortext{Autorid #1}}

% Title page
\renewcommand*{\maketitle}{%
	\begin{titlepage}
		\centering
		\fontsize{16}{12}
		\selectfont
		Viljandi Gümnaasium
		
		\vfill
		
		\fontsize{20}{12}
		\selectfont
		\textbf{\@title}
		
		\fontsize{16}{12}
		\selectfont
		
		\edef\1{\the\type}
		\ifx\1\empty
			\type{Uurimistöö}
		\fi
		
		\the\type
		
		\begin{flushright}
			\singlespacing
			\fontsize{16}{12}
			\selectfont
			\the\authortext
			
			\fontsize{14}{12}
			\selectfont
			\the\instructortext
		\end{flushright}
		
		\vfill
		
		\fontsize{16}{12}
		\selectfont
		Viljandi \@date
	\end{titlepage}
	
	% Titlepage keeps the page counter at 1, so we need to manually set it to 2.
	\setcounter{page}{2}%
}